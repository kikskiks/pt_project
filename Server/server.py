#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import socket
import sys
from time import sleep
from random import randint
# import logging

#  logging.basicConfig(format = u'%(levelname)-8s [%(asctime)s] %(message)s', level = logging.DEBUG, filename =
# u'server.log')


def main():
    message_recv = 0
    print ('SERVER is running...')
    print
    with open('server_conf.json') as json_conf_file:
        config = json.load(json_conf_file)
        port = (config['server']['port'])
        sock = socket.socket()
        sock.bind(('', int(port)))
        sock.listen(1)
        conn, addr = sock.accept()

    print('Client connected: ', addr)
    print
    while True:
        data = conn.recv(1024).decode()
        message_recv += 1
        print data, message_recv
        sleep(randint(1, 5))
        conn.send('Returned upper string from SERVER: ' + data.upper().encode())
        if not data:
            print ('NO DATA')
            sys.exit(0)
    conn.close()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print ('KeyboardInterrupt')
        sys.exit(0)
