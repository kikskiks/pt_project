#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import time


print ('comp is running...')
sock = socket.socket()
sock.bind(('', 9080))
sock.listen(1)
conn, addr = sock.accept()

print ('connected computer on: ', addr)

while True:
    data = conn.recv(1024).decode()
    print (data)
    time.sleep(1)
    if not data:
        print ('no data recv')
        break
conn.close()

input()
