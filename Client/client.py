#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import json
from random import randint
import sys
from time import sleep, time


# logging.basicConfig(format=u'%(levelname)-8s [%(asctime)s] %(message)s', level = logging.DEBUG,
# filename = u'mylog.log')


HOST = ''
PORT = 0

timer_end = ''
timer_start = ''

message_send_count = 0
message_recv_count = 0
disconnected_servers_count = 0

minimum_anwser_time = 0
maximym_anwser_time = 0
time_all_session = 0
middle_anwser_time = 0

print ('CLIENT is running...')
print
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


def parse_options():
    global HOST, PORT, timer_end, timer_start
    with open('client_conf.json') as json_conf_file:
        config = json.load(json_conf_file)
    HOST = (config['server']['ip'])
    PORT = int((config['server']['port']))
    timer_start = (config['timer_start'])
    timer_end = (config['timer_end'])
parse_options()


def ping_server():
    while True:
        s.send('ping'.encode())
        sleep(1)
        if OSError:
            print ('No available servers found')
        else:
            break


def data_sending():
    global message_send_count, message_recv_count, time_all_session, maximym_anwser_time, minimum_anwser_time, middle_anwser_time
    try:
        sleep(randint((int(timer_start)), (int(timer_end))))
        t1 = time()
        s.send('Message from CLIENT'.encode())
        message_send_count += 1
        data = s.recv(1024).decode()
        message_recv_count += 1
        middle_anwser_time = time_all_session / message_recv_count
        print data, message_recv_count
        time_one_request = time() - t1
        if minimum_anwser_time == 0 or minimum_anwser_time > time_one_request:
            minimum_anwser_time = time_one_request
        if maximym_anwser_time == 0 or maximym_anwser_time < time_one_request:
            maximym_anwser_time = time_one_request
        time_all_session += time_one_request
    except OSError:
        print('NO CONNECTIONS')
        input()


def connection_try():
    connected = False
    while not connected:
        try:
            s.connect((HOST, PORT))
            connected = True
        except:
            print ('No available servers found, trying to connect')


def connection():
    connection_try()
    while True:
        data_sending()
    s.close()


def main():
    connection()


def stats():
    print ('Messages sent = {}'.format(message_send_count))
    print ('Messages recv = {}'.format(message_recv_count))
    message_without_anwser = message_send_count - message_recv_count
    print ('Message without anwser = {}'.format(message_without_anwser))
    print ('MIDDLE anwser time = {}'.format(middle_anwser_time))
    print ('MIN anwser time = {}'.format(minimum_anwser_time))
    print ('MAX anwser time = {}'.format(maximym_anwser_time))
    print ('ALL session time = {}'.format(time_all_session))


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print
        print "KeyboardInterrupt"
        stats()
        sys.exit(0)
